#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{

}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer=&planeNormal;
    planeTranslationPointer=&planeTranslation;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer=&lineOrigin;
    lineTangentPointer=&lineTangent;

}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    double dotProductOrthogonality= planeNormalPointer->dot(*lineTangentPointer);
    if(abs(dotProductOrthogonality)<=toleranceParallelism) //siamo nel caso 0 o 1
    {
        //controllo se l'origine è nel piano
        if(abs(planeNormalPointer->dot(*lineOriginPointer)-*planeTranslationPointer)<=toleranceIntersection)
            intersectionType=Coplanar;
        else
           intersectionType=NoInteresection;
        return false; //chiedere il punto di intersezione a questa funzione non ha senso in questo caso
    }
    else
    {
        intersectionType=PointIntersection;
        intersectionParametricCoordinate=(*planeTranslationPointer-planeNormalPointer->dot(*lineOriginPointer))/planeNormalPointer->dot(*lineTangentPointer);
        return true;
    }
}
