#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{

}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(0)=planeNormal;
    rightHandSide(0)=planeTranslation;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1)=planeNormal;
    rightHandSide(1)=planeTranslation;

}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    tangentLine=matrixNomalVector.row(0).cross(matrixNomalVector.row(1));
    matrixNomalVector.row(2)=tangentLine;
    rightHandSide(2)=0;
    if(tangentLine.norm()<=toleranceParallelism)
    {
        if(abs(rightHandSide(0)-rightHandSide(1))<=toleranceIntersection)
            intersectionType=Coplanar;
        else
           intersectionType=NoInteresection;
        return false;
    }
    else
    {
        intersectionType=LineIntersection;
        pointLine=matrixNomalVector.colPivHouseholderQr().solve(rightHandSide);
        return true;
    }
}
